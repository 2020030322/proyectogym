package Modelo;

import Vista.AdminV;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import java.awt.Color;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class AdminM {

    Conexion cone = new Conexion();
    Connection con = cone.getConexion();
    Statement st;
    ResultSet rs;
    DefaultTableModel modelo;
    int idtrabajadorT;
    String nombreT;
    String apellidopT;
    String apellidomT;
    int estadoT;
    int idsocioS;
    String nombreS;
    String apellidopS;
    String apellidomS;
    String ntelS;
    String fechaS;
    String fregistroS;
    Date finicioS;
    Date fvencimientoS;
    int estadoS;

    public int getIdtrabajadorT() {
        return idtrabajadorT;
    }

    public void setIdtrabajadorT(int idtrabajadorT) {
        this.idtrabajadorT = idtrabajadorT;
    }

    public String getNombreT() {
        return nombreT;
    }

    public void setNombreT(String nombreT) {
        this.nombreT = nombreT;
    }

    public String getApellidopT() {
        return apellidopT;
    }

    public void setApellidopT(String apellidopT) {
        this.apellidopT = apellidopT;
    }

    public String getApellidomT() {
        return apellidomT;
    }

    public void setApellidomT(String apellidomT) {
        this.apellidomT = apellidomT;
    }

    public int getEstadoT() {
        return estadoT;
    }

    public void setEstadoT(int estadoT) {
        this.estadoT = estadoT;
    }

    public int getIdsocioS() {
        return idsocioS;
    }

    public void setIdsocioS(int idsocioS) {
        this.idsocioS = idsocioS;
    }

    public String getNombreS() {
        return nombreS;
    }

    public void setNombreS(String nombreS) {
        this.nombreS = nombreS;
    }

    public String getApellidopS() {
        return apellidopS;
    }

    public void setApellidopS(String apellidopS) {
        this.apellidopS = apellidopS;
    }

    public String getApellidomS() {
        return apellidomS;
    }

    public void setApellidomS(String apellidomS) {
        this.apellidomS = apellidomS;
    }

    public String getNtelS() {
        return ntelS;
    }

    public void setNtelS(String ntelS) {
        this.ntelS = ntelS;
    }

    public String getFechaS() {
        return fechaS;
    }

    public void setFechaS(String fechaS) {
        this.fechaS = fechaS;
    }

    public String getFregistroS() {
        return fregistroS;
    }

    public void setFregistroS(String fregistroS) {
        this.fregistroS = fregistroS;
    }

    public Date getFinicioS() {
        return finicioS;
    }

    public void setFinicioS(Date finicioS) {
        this.finicioS = finicioS;
    }

    public Date getFvencimientoS() {
        return fvencimientoS;
    }

    public void setFvencimientoS(Date fvencimientoS) {
        this.fvencimientoS = fvencimientoS;
    }

    public int getEstadoS() {
        return estadoS;
    }

    public void setEstadoS(int estadoS) {
        this.estadoS = estadoS;
    }

    public void fecha() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        //  System.out.println(formatter.format(date));
        this.setFechaS((String) formatter.format(date));

    }

    public void BuscarS(AdminV adv) throws SQLException {


        cone.getConexion();
        PreparedStatement ps = null;
        String sqlS = "SELECT * FROM gym.socios WHERE idsocio = ? or ntel=?";

        try {


            ps = (PreparedStatement) con.prepareStatement(sqlS);
            ps.setString(1, adv.txtidsocioS.getText());
            ps.setString(2, adv.txtntelS.getText());

            rs = ps.executeQuery();

            if (rs.next()) {
                //asignando resultado a las variables
                this.setIdsocioS(Integer.parseInt(rs.getString("idsocio")));

                setNombreS(rs.getString("nombre"));

                setApellidopS(rs.getString("apellidop"));

                setApellidomS(rs.getString("apellidom"));

                setNtelS(rs.getString("ntel"));

                setFregistroS(rs.getString("fregistro"));

                setFinicioS(rs.getDate("finicio"));

                setFvencimientoS(rs.getDate("fvencimiento"));

                setEstadoS(Integer.parseInt(rs.getString("estado")));

                //formato visual
                adv.txtidsocioS.setText(String.valueOf(this.idsocioS));

                adv.txtnombreS.setText(this.getNombreS());
                adv.txtapellidopS.setText(this.getApellidopS());
                adv.txtapellidomS.setText(this.getApellidomS());
                adv.txtntelS.setText(this.getNtelS());
                adv.txtfregistroS.setText(this.getFregistroS());
                //adv.txtfinicioS.setText(String.valueOf(this.getFinicioS()));
                adv.txtvencimientoS.setText(String.valueOf(this.getFvencimientoS()));
                if (this.getEstadoS() == 1) {
                    adv.lblestado1S.setText("Activo");
                    adv.lblestado1S.setForeground(Color.GREEN);
                }
                if (this.getEstadoS() == 0) {
                    adv.lblestado1S.setText("Baja");
                    adv.lblestado1S.setForeground(Color.red);
                }
                // this.limpiartxtS(adv);

            } else {
                JOptionPane.showMessageDialog(null, "Socio no encontrado");
                this.limpiartxtS(adv);
                //  System.out.println("entrando a trabadores");

            }


        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void limpiartxtS(AdminV adv) {

        adv.txtidsocioS.setText(null);
        adv.txtnombreS.setText(null);
        adv.txtapellidopS.setText(null);
        adv.txtapellidomS.setText(null);
        adv.txtntelS.setText(null);
        adv.txtfregistroS.setText(null);
 //       adv.txtfinicioS.setText(null);
        adv.txtvencimientoS.setText(null);
        adv.lblestado1S.setText(null);

    }

    public void membresias(AdminV adv) {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

        Date date = new Date(this.getFvencimientoS().getTime());
        String fe = ((String) formatter.format(date));
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(formatter.parse(fe));
        } catch (ParseException ex) {
            Logger.getLogger(AdminM.class.getName()).log(Level.SEVERE, null, ex);
        }
        c.add(Calendar.MONTH, Integer.parseInt("1"));
        fe = formatter.format(c.getTime());
        //  System.out.println("Fecha: " + fe);

//modificar la membresia

        cone.getConexion();
        PreparedStatement ps = null;
        String sqlS = "update gym.socios set fvencimiento= ? where idsocio = ? ";

        try {

            ps = (PreparedStatement) con.prepareStatement(sqlS);
            ps.setString(1, fe);
            ps.setString(2, adv.txtidsocioS.getText());
            int res = ps.executeUpdate();
            if (res > 0) {

                JOptionPane.showMessageDialog(null, "Agregado un mes a su membresia");

                this.BuscarS(adv);
            } else {
                JOptionPane.showMessageDialog(null, "Membresia No Modificada");
            }

        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, e + "Error de Modificacion");
        }


    }

    public void altabaja(AdminV adv) {
        cone.getConexion();
        PreparedStatement ps = null;
        String sqlS = "update gym.socios set estado= ? where idsocio = ? ";
        if (this.estadoS == 1) {
            try {

                ps = (PreparedStatement) con.prepareStatement(sqlS);
                ps.setInt(1, 0);
                ps.setString(2, adv.txtidsocioS.getText());
                int res = ps.executeUpdate();
                if (res > 0) {
                    JOptionPane.showMessageDialog(null, "Estatus Modificado");
                    this.BuscarS(adv);
                } else {
                    JOptionPane.showMessageDialog(null, "Estatus No Modificado");
                }


            } catch (Exception e) {

                JOptionPane.showMessageDialog(null, "Error de Activo/Inactivo");
            }
        } else {
            try {

                ps = (PreparedStatement) con.prepareStatement(sqlS);
                ps.setInt(1, 1);
                ps.setString(2, adv.txtidsocioS.getText());
                int res = ps.executeUpdate();
                if (res > 0) {
                    JOptionPane.showMessageDialog(null, "Estatus Modificado");
                    this.BuscarS(adv);
                } else {
                    JOptionPane.showMessageDialog(null, "Estatus No Modificado");
                }


            } catch (Exception e) {

                JOptionPane.showMessageDialog(null, "Error de Activo/Inactivo");
            }

        }
    }

    public void renovar(AdminV adv) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");

        Date date = new Date(System.currentTimeMillis());
        String fe = ((String) formatter.format(date));
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(formatter.parse(fe));
        } catch (ParseException ex) {
            Logger.getLogger(AdminM.class.getName()).log(Level.SEVERE, null, ex);
        }
        //  c.add(Calendar.MONTH, Integer.parseInt("1"));
        fe = formatter.format(c.getTime());
        //    System.out.println("Fecha: " + fe);

//modificar la membresia

        cone.getConexion();
        PreparedStatement ps = null;
        String sqlS = "update gym.socios set fvencimiento= ? where idsocio = ? ";

        try {

            ps = (PreparedStatement) con.prepareStatement(sqlS);
            ps.setString(1, fe);
            ps.setString(2, adv.txtidsocioS.getText());
            int res = ps.executeUpdate();
            if (res > 0) {

                JOptionPane.showMessageDialog(null, "Membresia Renovada");

                this.BuscarS(adv);
            } else {
                JOptionPane.showMessageDialog(null, "Membresia No Modificada");
            }
        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, "Error de Modificacion");
        }
    }

    public void BuscarT(AdminV adv) throws SQLException {


        cone.getConexion();
        PreparedStatement ps = null;
        String sqlS = "SELECT * FROM gym.trabajadores WHERE idtrabajadores = ?";

        try {

            ps = (PreparedStatement) con.prepareStatement(sqlS);
            ps.setString(1, adv.txtidtrabajador.getText());

            rs = ps.executeQuery();

            if (rs.next()) {
                //asignando resultado a las variables locales
                this.setIdtrabajadorT(Integer.parseInt(rs.getString("idtrabajadores")));

                setNombreT(rs.getString("nombre"));

                setApellidopT(rs.getString("apellidop"));

                setApellidomT(rs.getString("apellidom"));

                setEstadoT(Integer.parseInt(rs.getString("estado")));

                //formato visual(txtfield)
                adv.txtidtrabajador.setText(String.valueOf(this.idtrabajadorT));

                adv.txtnombreT.setText(this.getNombreT());
                adv.txtapellidopT.setText(this.getApellidopT());
                adv.txtapellidomT.setText(this.getApellidomT());

                if (this.getEstadoT() == 1) {
                    adv.lblestado1T.setText("Activo");
                    adv.lblestado1T.setForeground(Color.GREEN);
                }
                if (this.getEstadoT() == 0) {
                    adv.lblestado1T.setText("Baja");
                    adv.lblestado1T.setForeground(Color.red);
                }


            } else {
                JOptionPane.showMessageDialog(null, "Trabajador no encontrado");
                this.limpiartxtS(adv);
                //  System.out.println("entrando a trabadores");

            }


        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void altabajat(AdminV adv) {

        cone.getConexion();
        PreparedStatement ps = null;
        String sqlT = "update gym.trabajadores set estado= ? where idtrabajadores = ? ";
        if (this.estadoT == 1) {
            try {

                ps = (PreparedStatement) con.prepareStatement(sqlT);
                ps.setInt(1, 0);
                ps.setString(2, adv.txtidtrabajador.getText());
                int res = ps.executeUpdate();
                if (res > 0) {
                    JOptionPane.showMessageDialog(null, "Estatus Modificado");
                    this.BuscarT(adv);
                } else {
                    JOptionPane.showMessageDialog(null, "Estatus No Modificado");
                }


            } catch (Exception e) {

                JOptionPane.showMessageDialog(null, "Error de Activo/Inactivo");
            }
        } else {
            try {

                ps = (PreparedStatement) con.prepareStatement(sqlT);
                ps.setInt(1, 1);
                ps.setString(2, adv.txtidtrabajador.getText());
                int res = ps.executeUpdate();
                if (res > 0) {
                    JOptionPane.showMessageDialog(null, "Estatus Modificado");
                    this.BuscarT(adv);
                } else {
                    JOptionPane.showMessageDialog(null, "Estatus No Modificado");
                }


            } catch (Exception e) {

                JOptionPane.showMessageDialog(null, "Error de Activo/Inactivo");
            }

        }
    }

    public void limpiartxtT(AdminV adv) {
        adv.txtidtrabajador.setText(null);
        adv.txtnombreT.setText(null);
        adv.txtapellidopT.setText(null);
        adv.txtapellidomT.setText(null);
        adv.lblestado1T.setText(null);
    }

    public void agregart(AdminV adv) throws SQLException {

        adv.txtidtrabajador.setText(null);

        if (!"".equals(adv.txtnombreT.getText()) && !"".equals(adv.txtapellidopT.getText()) && !"".equals(adv.txtapellidomT.getText())) {
            //si los campos estan llenos entonces agregamos al trabajador
            String sql = "call gym.agregartrabajador(?, ?, ?)";
            PreparedStatement ps = null;
            try {
                ps = (PreparedStatement) con.prepareStatement(sql);
                ps.setString(1, adv.txtnombreT.getText());
                ps.setString(2, adv.txtapellidopT.getText());
                ps.setString(3, adv.txtapellidomT.getText());

                rs = ps.executeQuery();
                JOptionPane.showMessageDialog(null, "Trabajador Agregado");

                //regresar el id
                String ultimo = "select * from gym.trabajadores where idtrabajadores = (SELECT MAX(idtrabajadores) from trabajadores)";
                //  PreparedStatement ps1 = (PreparedStatement) con.prepareStatement(sql);
                st = con.createStatement();
                rs = st.executeQuery(ultimo);
                if (rs.next()) {
                    //variables local
                    this.setIdtrabajadorT(rs.getInt("idtrabajadores"));
                    this.setNombreT(rs.getString("nombre"));
                    this.setApellidopT(rs.getString("apellidop"));
                    this.setApellidomT(rs.getString("apellidom"));
                    this.setEstadoT(rs.getInt("estado"));
                    //visual
                    adv.txtidtrabajador.setText(String.valueOf(this.getIdtrabajadorT()));
                    adv.txtnombreT.setText(this.getNombreT());
                    adv.txtapellidopT.setText(this.getApellidopT());
                    adv.txtapellidomT.setText(this.getApellidomT());

                    if (this.getEstadoT() == 1) {
                        adv.lblestado1T.setText("Activo");
                        adv.lblestado1T.setForeground(Color.GREEN);
                    }
                    if (this.getEstadoT() == 0) {
                        adv.lblestado1T.setText("Baja");
                        adv.lblestado1T.setForeground(Color.red);
                    }


                }

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
                Logger.getLogger(AdminM.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            //en caso de que falte algun dato
            JOptionPane.showMessageDialog(null, "Faltan datos para el registro");
        }
    }

    public void modificart(AdminV adv) {

        cone.getConexion();
        PreparedStatement ps = null;
        String mod = "update gym.trabajadores set nombre = ?, apellidop = ?, apellidom = ? where idtrabajadores = ? ";
        if (!"".equals(adv.txtidtrabajador.getText()) && !"".equals(adv.txtnombreT.getText()) && !"".equals(adv.txtapellidopT.getText()) && !"".equals(adv.txtapellidomT.getText())) {
            //si los campos estan llenos entonces modificamos al trabajador
            try {
                ps = (PreparedStatement) con.prepareStatement(mod);
                ps.setString(1, adv.txtnombreT.getText());
                ps.setString(2, adv.txtapellidopT.getText());
                ps.setString(3, adv.txtapellidomT.getText());
                ps.setString(4, adv.txtidtrabajador.getText());
                int res = ps.executeUpdate();
                if (res > 0) {
                    JOptionPane.showMessageDialog(null, "Datos Personales Modificados");
                    this.BuscarT(adv);
                } else {
                    JOptionPane.showMessageDialog(null, "Datos Personales No Modificado");
                }
            } catch (SQLException ex) {
                Logger.getLogger(AdminM.class.getName()).log(Level.SEVERE, null, ex);
            }


        } else {
            JOptionPane.showMessageDialog(null, "Faltan datos para completar la modificacion");
        }
    }

    public void agregars(AdminV adv) throws SQLException {

        adv.txtidsocioS.setText(null);

        if (!"".equals(adv.txtnombreS.getText()) && !"".equals(adv.txtapellidopS.getText()) && !"".equals(adv.txtapellidomS.getText()) && !"".equals(adv.txtntelS.getText())) {
            //si los campos estan llenos entonces agregamos al trabajador
            //call gym.agregarsocio('Xiomara', 'Vargas', 'nose', '6691616161', now(),now(), now(), 1);
            String sql = "call gym.agregarsocio(?,?,?,?, now(),null, now(), 1);";
            PreparedStatement ps = null;
            try {
                ps = (PreparedStatement) con.prepareStatement(sql);
                ps.setString(1, adv.txtnombreS.getText());
                ps.setString(2, adv.txtapellidopS.getText());
                ps.setString(3, adv.txtapellidomS.getText());
                ps.setString(4, adv.txtntelS.getText());
                
                rs = ps.executeQuery();
                JOptionPane.showMessageDialog(null, "Socio Agregado");

                //regresar el id
                String ultimo = "select * from gym.socios where idsocio = (SELECT MAX(idsocio) from socios)";
                //  PreparedStatement ps1 = (PreparedStatement) con.prepareStatement(sql);
                st = con.createStatement();
                rs = st.executeQuery(ultimo);
                if (rs.next()) {
                    //variables local
                    this.setIdsocioS(rs.getInt("idsocio"));
                    this.setNombreS(rs.getString("nombre"));
                    this.setApellidopS(rs.getString("apellidop"));
                    this.setApellidomS(rs.getString("apellidom"));
                    this.setFregistroS(rs.getString("fregistro"));
                    this.setFinicioS(rs.getDate("finicio"));
                    this.setFvencimientoS(rs.getDate("fvencimiento"));
                    this.setEstadoS(rs.getInt("estado"));
                    //visual
                    adv.txtidsocioS.setText(String.valueOf(this.getIdsocioS()));
                    adv.txtnombreS.setText(this.getNombreS());
                    adv.txtapellidopS.setText(this.getApellidopS());
                    adv.txtapellidomS.setText(this.getApellidomS());
                    adv.txtntelS.setText(this.getNtelS());
                    adv.txtfregistroS.setText(String.valueOf(this.getFregistroS()));
                    adv.txtvencimientoS.setText(String.valueOf(this.getFvencimientoS()));
                    if (this.getEstadoS() == 1) {
                        adv.lblestado1S.setText("Activo");
                        adv.lblestado1S.setForeground(Color.GREEN);
                    }
                    if (this.getEstadoS() == 0) {
                        adv.lblestado1S.setText("Baja");
                        adv.lblestado1S.setForeground(Color.red);
                    }


                }

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex);
                Logger.getLogger(AdminM.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            //en caso de que falte algun dato
            JOptionPane.showMessageDialog(null, "Faltan datos para el registro");
        }
    }
     public void modificars(AdminV adv) {

        cone.getConexion();
        PreparedStatement ps = null;
        String mod = "update gym.socios set nombre = ?, apellidop = ?, apellidom = ?, ntel = ? where idsocio = ? ";
        if (!"".equals(adv.txtidsocioS.getText()) && !"".equals(adv.txtnombreS.getText()) && !"".equals(adv.txtapellidopS.getText()) && !"".equals(adv.txtapellidomS.getText())&& !"".equals(adv.txtntelS.getText())) {
            //si los campos estan llenos entonces modificamos al trabajador
            try {
                ps = (PreparedStatement) con.prepareStatement(mod);
                ps.setString(1, adv.txtnombreS.getText());
                ps.setString(2, adv.txtapellidopS.getText());
                ps.setString(3, adv.txtapellidomS.getText());
                ps.setString(4, adv.txtntelS.getText());
                ps.setString(5,adv.txtidsocioS.getText());
                int res = ps.executeUpdate();
                if (res > 0) {
                    JOptionPane.showMessageDialog(null, "Datos Personales Modificados");
                    this.BuscarS(adv);
                } else {
                    JOptionPane.showMessageDialog(null, "Datos Personales No Modificado");
                }
            } catch (SQLException ex) {
                Logger.getLogger(AdminM.class.getName()).log(Level.SEVERE, null, ex);
            }


        } else {
            JOptionPane.showMessageDialog(null, "Faltan datos para completar la modificacion");
        }
    }
}
