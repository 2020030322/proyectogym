package Modelo;

import Vista.VistaV;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import java.awt.HeadlessException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class VistaM {

    Conexion cone = new Conexion();
    Connection con = cone.getConexion();
    Statement st;
    ResultSet rs;
    DefaultTableModel modelo;
    private int id;
    private int idt;
    String fecha = null;
    private String contraseña;
    private String nombre;
    private String apellidop;
    private String apellidom;
    //  private String fecha;
    private String ntel;
    private String fregistro;
    private String finicio;
    private String fvencimiento;
    private String estado;
    int op = 1;

    public int getIdt() {
        return idt;
    }

    public void setIdt(int idt) {
        this.idt = idt;
    }

    public String getNtel() {
        return ntel;
    }

    public void setNtel(String ntel) {
        this.ntel = ntel;
    }

    public String getFregistro() {
        return fregistro;
    }

    public void setFregistro(String fregistro) {
        this.fregistro = fregistro;
    }

    public String getFinicio() {
        return finicio;
    }

    public void setFinicio(String finicio) {
        this.finicio = finicio;
    }

    public String getFvencimiento() {
        return fvencimiento;
    }

    public void setFvencimiento(String fvencimiento) {
        this.fvencimiento = fvencimiento;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int GetId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidop() {
        return apellidop;
    }

    public void setApellidop(String apellidop) {
        this.apellidop = apellidop;
    }

    public String getApellidom() {
        return apellidom;
    }

    public void setApellidom(String apellidom) {
        this.apellidom = apellidom;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    //////////////////////
    public void Buscar(VistaV vis) throws SQLException {


        cone.getConexion();
        PreparedStatement ps = null;
        String sqlT = "SELECT * FROM gym.socios WHERE idsocio = ? nombre = ? and apellidop = ? and apellidom = ? and ntel = ? and fregistro = ? and finicio = ? and fvencimiento = ? and estado= 1";
        String sqlS = "SELECT * FROM gym.socios WHERE idsocio = ? and ntel = ? and estado= 1";
        // op = 1;
        if (op == 1) {//buacar socio

            try {
                ps = (PreparedStatement) con.prepareStatement(sqlS);




                rs = ps.executeQuery();

                if (rs.next()) {

                    setNombre(rs.getString("nombre"));

                    setApellidop(rs.getString("apellidop"));

                    setApellidom(rs.getString("apellidom"));

                    setNtel(rs.getString("ntel"));

                    setFregistro(rs.getString("fregistro"));

                    setFinicio(rs.getString("finicio"));

                    setFvencimiento(rs.getString("fvencimiento"));

                    setEstado(rs.getString("estado"));

                    //   this.aviso();
                    this.setIdt(0);

                    this.agregarregistro();
                    this.ActualizarF(vis);


                } else {
                    JOptionPane.showMessageDialog(null, "ID y/o Contraseña incorrecta");
                    //  System.out.println("entrando a trabadores");
                    op = 2;
                }


            } catch (Exception e) {

                JOptionPane.showMessageDialog(null, e);
            }
        }
        if (op == 2) {

            //  System.out.println("entro!");
            try {


                ps = (PreparedStatement) con.prepareStatement(sqlT);




                rs = ps.executeQuery();

                if (rs.next()) {


                    setNombre(rs.getString("nombre"));

                    setApellidop(rs.getString("apellidop"));

                    setApellidom(rs.getString("apellidom"));

                    setNtel(rs.getString("ntel"));

                    setFregistro(rs.getString("fregistro"));
                    
                    setFinicio(rs.getString("finicio"));

                    setFvencimiento(rs.getString("fvencimiento"));

                    setEstado(rs.getString("estado"));
                    this.setId(0);
                    this.agregarregistro();
                    this.ActualizarF(vis);


                } else {
                    //  JOptionPane.showMessageDialog(null, "Trabajador No existe");
                    //  System.out.println("entrando a trabadores");
                    op = 1;
                }


            } catch (Exception e) {

                JOptionPane.showMessageDialog(null, e);
            }


        }

    }

    private void agregarregistro() {

        cone.getConexion();

        //  SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        //Date date = new Date(System.currentTimeMillis());
        //System.out.println(formatter.format(date));
        //String fecha = formatter.format(date);

        try {

            
            String sql = "INSERT INTO gym.socios (`idsocio`,`nombre`,`apellidop`,`apellidom`,`ntel`,`fregistro`,`finicio`,`fvencimineto`,`estado`) values ('" + this.GetId() + "','" + getNombre() + "','" + getApellidop() + "','" + getApellidom() +"','"+ getNtel()+"','"+ getFregistro()+"','"+ /*getFinicio()+"','"+*/ getFvencimiento()+ "','" + estado + "')";
          
            st = con.createStatement();

            st.executeUpdate(sql);

            JOptionPane.showMessageDialog(null, "Socio Registrado con Exito");





        } catch (HeadlessException | SQLException e) {
            JOptionPane.showMessageDialog(null, "Error de Insertar");
        }


    }

    public void consultarF(VistaV vis) {
        String sql = "select * from gym.socios";

        try {
            cone.getConexion();
            st = con.createStatement();
            rs = st.executeQuery(sql);

            Object[] visita = new Object[8];
            modelo = (DefaultTableModel) vis.TablaVisitas.getModel();
            while (rs.next()) {
                visita[0] = rs.getInt("idsocio");
                visita[1] = rs.getString("nombre");
                //  visita[2] = rs.getString("idtrabajador");
                visita[2] = rs.getString("apellidop");
                visita[3] = rs.getString("apellidom");
                visita[4] = rs.getString("ntel");
                visita[5] = rs.getString("fregistro");
                //visita[6] = rs.getString("finicio");
                visita[6] = rs.getString("fvencimiento");
                visita[7] = rs.getString("estado");

                modelo.addRow(visita);
            }

            vis.TablaVisitas.setModel(modelo);
        } catch (Exception e) {
              JOptionPane.showMessageDialog(null,e);
        }
    }

    void limpiarTabla(DefaultTableModel model, VistaV vis) {
        for (int i = 0; i < modelo.getRowCount(); i++) {
            model.removeRow(i);
            i = i - 1;
        }

    }

    public void ActualizarF(VistaV vis) {
        this.limpiarTabla(modelo, null);
        this.consultarF(vis);
    }

    public void fecha() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        //  System.out.println(formatter.format(date));
        this.setFecha((String) formatter.format(date));

    }
}
