create database gym;
use gym;
/*Alexis y yennifer*/
create table trabajadores(
idtrabajadores int auto_increment primary key,
nombre varchar(50),
apellidop varchar(50),
apellidom varchar(50),
estado boolean
);

create table socios(
idsocio int auto_increment primary key,
nombre varchar(50),
apellidop varchar(50),
apellidom varchar(50),
ntel varchar(15),
fregistro date,
finicio date,
fvencimiento date,
estado boolean 
);

create table visitas(
idregistro int auto_increment primary key,
idsocio int,
nombre varchar(50),
apellidop varchar(50),
apellidom varchar(50),
fh varchar(50),
idtrabajador int
);

/*Registros de movimientos triggers Ivan Gomez y Alexis*/
create table rtrabajadores(
idtrabajadores_c int,
nombre_c varchar(50),
apellidop_c varchar(50),
apellidom_c varchar(50),
estado_c boolean,
accion varchar(20)
);

create table rsocios(
idsocio_c int,
nombre_c varchar(50),
apellidop_c varchar(50),
apellidom_c varchar(50),
ntel_c varchar(15),
fregistro_c date,
finicio_c date,
fvencimiento_c date,
estado_c boolean,
accion varchar(20) 
);

create table rvisitas(
idregistro_c int,
idsocio_c int,
nombre_c varchar(50),
apellidop_c varchar(50),
apellidom_c varchar(50),
fh_c varchar(50),
idtrabajador int,
accion varchar(20)
);

/*Triggers Pedro*/ 
DELIMITER \\
CREATE TRIGGER `tr_insertar_socio` after INSERT ON socios FOR EACH ROW
BEGIN
   INSERT INTO rsocios values(new.idsocio,new.nombre,new.apellidop,new.apellidom,new.ntel,new.fregistro,new.finicio,new.fvencimiento,new.estado,"Agregado");
END \\
DELIMITER ;

DELIMITER \\
CREATE TRIGGER `tr_actualizar_socio` BEFORE update ON socios FOR EACH ROW
BEGIN
   INSERT INTO rsocios values(old.idsocio,new.nombre,new.apellidop,new.apellidom,new.ntel,new.fregistro,new.finicio,old.fvencimiento,new.estado,"Modificado");
END \\
DELIMITER ;
/*drop trigger tr_actualizar_socio;*/
DELIMITER \\
CREATE TRIGGER `tr_insertar_trabajador` after INSERT ON trabajadores FOR EACH ROW
BEGIN
   INSERT INTO rtrabajadores values(new.idtrabajadores,new.nombre,new.apellidop,new.apellidom,new.estado,"Agregado");
END \\
DELIMITER ;

DELIMITER \\
CREATE TRIGGER `tr_actualizar_trabajador` BEFORE update ON trabajadores FOR EACH ROW
BEGIN
   INSERT INTO rtrabajadores values(old.idtrabajadores,new.nombre,new.apellidop,new.apellidom,new.estado,"Modificado");
END \\
DELIMITER ;

set SQL_SAFE_UPDATES = 0;

/*Precedimiento almacenado Pedro*/
DELIMITER $$
CREATE PROCEDURE `agregarsocio` (in nombre varchar(50),in pellidop varchar(50),in apellidom varchar(50),in ntel varchar(15),in fregistro date,in finicio date,in fvencimiento date,in estado tinyint(1))
BEGIN
	 INSERT INTO `gym`.`socios` (`nombre`, `apellidop`, `apellidom`, `ntel`, `fregistro`, `finicio`, `fvencimiento`, `estado`) values(nombre,pellidop,apellidom,ntel,fregistro,finicio,fvencimiento,estado);
END$$
DELIMITER ;
call agregarsocio("Osmar","Sanchez","Guerrero","6692999999","2022-08-21","2022-08-21","2022-10-21",1);
call agregarsocio("Alexis","Flores","Prado","6692","2022-09-12","2022-08-21","2022-10-21",1);
call agregarsocio("Yenni","Rodriguez","Sanchez","6692","2022-09-12",null,null,1);
call gym.agregarsocio('Xiomara', 'Vargas', 'nose', '6691616161', now(),now(), now(), 1);
DELIMITER $$
CREATE PROCEDURE `agregartrabajador` (in nombre varchar(50),in apellidop varchar(50),in apellidom varchar(50))
BEGIN
	 INSERT INTO `gym`.`trabajadores` (`nombre`, `apellidop`, `apellidom`,`estado`) values(nombre,apellidop,apellidom,1);
END$$
DELIMITER ;
/* DROP PROCEDURE agregartrabajador;*/
call agregartrabajador("Miriam","Sanchez","Sanchez");




